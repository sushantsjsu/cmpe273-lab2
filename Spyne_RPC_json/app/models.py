import operator
from datetime import datetime
import re

import requests


def construct_get_call_with_agrs(uri, args):
    argument = ''
    first = True
    for k, v in args.items():
        if first:
            argument += '%s=%s' % (k, v)
            first = False
        else:
            argument += '&' + ('%s=%s' % (k, v))

    url = uri + '?' + argument
    return url


def make_get_call(url):
    return requests.get(url)


def make_get_call_with_args(uri, args):
    return requests.get(construct_get_call_with_agrs(uri, args))


def remove_block_of_and_split(address):
    address = re.sub(r"[0-9]*\s\bBLOCK OF\b\s", "", address)
    addreses = address.replace("ST &", "ST\n").split('\n', 1)
    return addreses


def increment_crime_at_address(crime, crime_at_address_count):
    address = crime['address']
    addresses = remove_block_of_and_split(address)
    for address in addresses:
        addr = address.strip()
        if addr in crime_at_address_count:
            crime_at_address_count[addr] = (crime_at_address_count.get(addr) + 1)
        else:
            crime_at_address_count[addr] = 1


def increment_crime_type_count(crime, crime_type_count):
    crime_type = crime['type']
    if crime_type in crime_type_count:
        crime_type_count[crime_type] = crime_type_count.get(crime_type) + 1
    else:
        crime_type_count[crime_type] = 1


def increment_crime_time_count(crime, crime_time_count):
    hour = datetime.strptime(crime['date'], '%m/%d/%y %I:%M %p').hour

    if 0 < hour <= 3:
        crime_time_count['12:01am-3am'] += 1
    elif 3 < hour <= 6:
        crime_time_count['3:01am-6am'] += 1
    elif 6 < hour <= 9:
        crime_time_count['6:01am-9am'] += 1
    elif 9 < hour <= 12:
        crime_time_count['9:01am-12noon'] += 1
    elif 12 < hour <= 15:
        crime_time_count['12:01pm-3pm'] += 1
    elif 15 < hour <= 18:
        crime_time_count['3:01pm-6pm'] += 1
    elif 18 < hour <= 21:
        crime_time_count['6:01pm-9pm'] += 1
    else:
        crime_time_count['9:01pm-12midnight'] += 1


def get_top_crime_address(num, crime_at_address_count):
    sorted_crime_at_address = sorted(crime_at_address_count.items(), key=operator.itemgetter(1), reverse=True)
    top_crime_locations = []
    num = min(num, len(sorted_crime_at_address))
    while num > 0:
        top_crime_locations.append(sorted_crime_at_address[num][0])
        num -= 1
    return top_crime_locations
