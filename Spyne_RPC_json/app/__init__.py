from spyne import Application
from spyne.protocol.http import HttpRpc
from spyne.protocol.json import JsonDocument
from app import views

application = Application([views.CrimeService],
                          tns='spyne.crime.service',
                          in_protocol=HttpRpc(validator='soft'),
                          out_protocol=JsonDocument()
                          )
