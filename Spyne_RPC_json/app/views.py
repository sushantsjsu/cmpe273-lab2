import logging
from spyne import srpc, ServiceBase, Unicode, Float
from app import models

logging.basicConfig(level=logging.DEBUG)


class CrimeService(ServiceBase):
    @srpc(Float, Float, Float, _returns=Unicode)
    def checkcrime(lat, lon, radius):
        crime_at_address_count = {}
        crime_type_count = {}
        crime_time_count = {'12:01am-3am': 0, '3:01am-6am': 0, '6:01am-9am': 0, '9:01am-12noon': 0, '12:01pm-3pm': 0,
                            '3:01pm-6pm': 0, '6:01pm-9pm': 0, '9:01pm-12midnight': 0}

        url = models.construct_get_call_with_agrs('https://api.spotcrime.com/crimes.json',
                                                  dict(lat=lat, lon=lon, radius=radius, key='.'))
        response = models.make_get_call(url)
        crimes = response.json()['crimes']

        for crime in crimes:
            models.increment_crime_at_address(crime, crime_at_address_count)
            models.increment_crime_type_count(crime, crime_type_count)
            models.increment_crime_time_count(crime, crime_time_count)

        top_crime_locations = models.get_top_crime_address(3, crime_at_address_count)

        result = {'total_crime': len(crimes),
                  'the_most_dangerous_streets': top_crime_locations,
                  'crime_type_count': crime_type_count,
                  'event_time_count': crime_time_count}

        return result